import Vue from "vue";
import VueRouter from "vue-router";
import RandomColors from "../views/RandomColors.vue";
import Home from "../views/Home.vue";
import Double from "../views/Double.vue";
import WordCounter from "../views/WordCounter.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/randomColors",
    name: "RandomColors",
    component: RandomColors
  },
  {
    path: "/growingPlants",
    name: "GrowingPlants",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/GrowingPlants.vue")
  },
  {
    path: "/double",
    name: "Double",
    component: Double
  },
  {
    path: "/wordCounter",
    name: "WordCounter",
    component: WordCounter
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
